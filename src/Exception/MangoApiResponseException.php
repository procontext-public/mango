<?php

namespace Procontext\MangoApi\Exception;

use Throwable;

class MangoApiResponseException extends MangoApiException
{
    protected $response;

    public function __construct($response = [], $message = 'Ошибка ответа Mango API', $code = 500, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    public function getResponse(): array
    {
        return $this->response;
    }
}