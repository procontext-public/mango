<?php

namespace Procontext\MangoApi\Exception;

use Throwable;

class ValidationException extends MangoApiException
{
    protected $messageBag;

    public function __construct($messageBag = [], $message = 'Ошибка валидации параметров Mango API', $code = 400, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->messageBag = $messageBag;
    }

    public function getMessageBag(): array
    {
        return $this->messageBag;
    }
}

